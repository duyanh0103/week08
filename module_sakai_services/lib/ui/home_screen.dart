import 'package:flutter/material.dart';
import '../model/site.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    List<Site> siteData = ModalRoute
        .of(context)!
        .settings
        .arguments as List<Site>;

    return Scaffold(
      appBar: AppBar(
        title: Text('xLMS client'),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
            Color.fromARGB(255, 181, 63, 146),
            Color.fromARGB(255, 234, 83, 254),
            Color.fromARGB(255, 255, 77, 225),
            Color.fromARGB(255, 183, 58, 166)
          ])),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 10.0),
            child: IconButton(
              onPressed: () {
                Navigator.of(context).pushReplacementNamed('/', arguments: 123);
              },
              icon: Icon(Icons.logout),
            ),
          ),
        ],
      ),
      body: Container(
        child: ListView.builder(
          itemCount: siteData.length,
          itemBuilder: (BuildContext context, int index) {
            var itemData = siteData[index];
            return ListTile(
              title: Text(itemData.title),
              // lưu ý
              subtitle: Text("${itemData.owner }"),
            );
          },
        ),
      ),
    );
  }
}
