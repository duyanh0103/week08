import 'package:flutter/material.dart';
import 'home_screen.dart';
import 'login_screen.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (context) => LoginScreen(),
        '/HomeScreen': (context) => HomeScreen(),
      },
      initialRoute: '/',
    );
  }
}
