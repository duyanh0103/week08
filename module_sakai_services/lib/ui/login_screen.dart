import 'dart:convert';

import 'package:flutter/material.dart';
import '../sakai_services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'home_screen.dart';
import 'package:http/http.dart' as http;
import '../model/site.dart';

class LoginScreen extends StatefulWidget {
  static const route = '/login';
  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  final sakaiServices = SakaiService(sakaiUrl: 'https://xlms.myworkspace.vn');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 146, 68, 142),
        title: Text('Login'),
      ),
      body: buildLoginForm(),
    );
  }

  Widget buildLoginForm() {
    return Form(
      key: formKey,
      child: Column(
        children: [usernameField(), passwordField(), loginButton()],
      ),
    );
  }

  Widget usernameField() {
    return TextFormField(
      controller: usernameController,
      decoration:
          InputDecoration(icon: Icon(Icons.person), labelText: 'username'),
      // validator: validateusername,
    );
  }

  Widget passwordField() {
    return TextFormField(
      controller: passwordController,
      obscureText: true,
      decoration:
          InputDecoration(icon: Icon(Icons.lock), labelText: 'Password'),
      // validator: validatePassword,
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () async {
          final username = usernameController.text;
          final password = passwordController.text;

          http.Response response =
              await sakaiServices.authenticate(username, password);
          if (response.statusCode == 200 || response.statusCode == 201) {
            var res = await sakaiServices.getSites();
            //decode json
            var jsonSite = json.decode(res.body);
            // make list with site_collection
            List jsonSite_collection = jsonSite['site_collection'];

            // intial list sites
            List<Site> sites = [];
            jsonSite_collection.forEach((e) {
              Site mySite = Site(
                  id: e['entityId'],
                  title: e['entityTitle'],
                  owner: e['siteOwner']['userDisplayName']);
              sites.add(mySite);
            });
            // take this argument and let the modalRoute find the context of it

            
            Navigator.of(context).pushReplacementNamed('/HomeScreen',arguments: sites);
            setState(() {});
          } else {
            Fluttertoast.showToast(
              msg: 'Wrong username or password!',
              fontSize: 15,
              backgroundColor: Color.fromARGB(255, 243, 33, 33),
            );
          }
        },
        child: Text('Login'));
  }
}
