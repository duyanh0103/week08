const keyId = 'entityId';
const keyTitle = 'entityTitle';
const keyOwner = 'siteOwner';
const keyDisplayName = 'userDisplayName';

class Site {
  late String id;
  late String title;
  late String owner;

  Site({required this.id, required this.title, required this.owner});

  Site.fromJson(Map<String, dynamic> json) {
    this.id = json[keyId] ?? '';
    this.title = json[keyTitle] ?? '';
    this.owner = json[keyOwner][keyDisplayName] ?? '';
  }
}
